include(enable_warnings)
include(cpp_check)
include(enable_sanitizers)
#USE_STANDART
include(with_math)

target_compile_definitions(${PROJECT_NAME} PRIVATE __USE_MINGW_ANSI_STDIO=1)
