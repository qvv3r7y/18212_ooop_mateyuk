#ifndef TASK_2_SRC_SCHEME_H_
#define TASK_2_SRC_SCHEME_H_

#include "WorkerFactory.h"
#include "WorkflowExceptions.h"

#include <vector>
#include <map>
#include <memory>

class Scheme {
public:
	Scheme() = default;
	Scheme(std::map<int, std::shared_ptr<IWorker>> &blocks, std::vector<int> &scheme);
	std::map<int, std::shared_ptr<IWorker>> getBlocks();
	std::vector<int> getScheme();
	void control(const std::string &input_filename, const std::string &output_filename);
private:
	void process(std::string &buffer);
	std::map<int, std::shared_ptr<IWorker>> blocks_;
	std::vector<int> scheme_;
};

#endif //TASK_2_SRC_SCHEME_H_
