#include "IParser.h"
#include "WorkflowExceptions.h"

#include <sstream>
#include <fstream>

Parser::Parser() {
	validators.push_back(new ReadFileValidator);
	validators.push_back(new WriteFileValidator);
	validators.push_back(new GrepValidator);
	validators.push_back(new SortValidator);
	validators.push_back(new ReplaceValidator);
	validators.push_back(new DumpValidator);
}

Parser::~Parser() {
	for (auto validator : validators) {
		delete validator;
		validator = nullptr;
	}
}

Scheme Parser::parse(std::string const &name_file) {
	std::ifstream in;
	std::string str;

	try {
		in.open(name_file);
	}
	catch (std::ifstream::failure &) {
		throw WorkflowException("Failure opened '" + name_file + "'");
	}

	try {
		std::getline(in, str);
	}
	catch (std::ifstream::failure &) {
		throw WorkflowException("Failure reading '" + name_file + "'");
	}

	if (str != "desc") {
		throw WorkflowException("Didn't find 'desc'");
	}
	get_blocks(in, name_file);
	get_scheme(in, name_file);
	return Scheme(worker_blocks, scheme);
}

std::map<int, std::shared_ptr<IWorker>> Parser::get_blocks(std::ifstream &in, const std::string &name) {
	WorkerFactory worker_factory; /// think about it
	std::string str;
	try {
		std::getline(in, str);
	}
	catch (std::ifstream::failure &) {
		throw WorkflowException("Failure read '" + name + "'");
	}
	while (!in.eof() && str != "csed") {
		WorkerMetaModel block(str);

		auto validator = validators.begin();
		while (validator != validators.end()) {
			if ((*validator)->isExist(block)) break;
			validator++;
		}
		if (validator == validators.end()) {
			throw WorkflowException("block does't exist");
		}
		if ((*validator)->isValid(block)) {
			if (worker_blocks.find(block.getId()) != worker_blocks.end()) {
				throw WorkflowException("repeat id");
			} else {
				worker_blocks[block.getId()] = worker_factory.createWorker(block.getType(), block.getArgs());
			}
		} else {
			throw WorkflowException("bad arguments");
		}
		std::getline(in, str);
	}
	if (str != "csed") {
		throw WorkflowException("no 'csed'");
	}
	return worker_blocks;
}
std::vector<int> Parser::get_scheme(std::ifstream &in, const std::string &name) {
	std::string str;
	try {
		std::getline(in, str);
	}
	catch (std::ifstream::failure &) {
		throw WorkflowException("Failure read '" + name + "'");
	}
	std::istringstream str_buf(str);

	char delimiter[2];
	int id = 0;


	if (!(str_buf >> id)) return scheme;
	scheme.push_back(id);

	while (!str_buf.eof()) {
		for (char &i : delimiter) {
			str_buf >> i;
		}
		if (delimiter[0] != '-' || delimiter[1] != '>') {
			throw WorkflowException("Bad delimiter. Use ->");
		}
		if (!(str_buf >> id)) {
			throw WorkflowException("Bad id");
		}
		scheme.push_back(id);
	}
	return scheme;
}
