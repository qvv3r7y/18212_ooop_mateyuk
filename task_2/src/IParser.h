#ifndef TASK_2_SRC_IPARSER_H_
#define TASK_2_SRC_IPARSER_H_

#include <string>
#include <unordered_map>
#include <vector>


#include "IValidator.h"
#include "Scheme.h"

class IParser {
public:
	virtual Scheme parse(std::string const &file) = 0;
};

class Parser : public IParser {
public:
	Parser();
	~Parser();
	Scheme parse(std::string const &name_file) override;
	std::map<int, std::shared_ptr<IWorker>> get_blocks(std::ifstream &, const std::string &);
	std::vector<int> get_scheme(std::ifstream &, const std::string &);
private:
	std::map<int, std::shared_ptr<IWorker>> worker_blocks;
	std::vector<int> scheme;
	std::vector<IValidator *> validators;
};

#endif //TASK_2_SRC_IPARSER_H_
