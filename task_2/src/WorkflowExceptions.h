#ifndef TASK_2_SRC_WORKFLOWEXCEPTIONS_H_
#define TASK_2_SRC_WORKFLOWEXCEPTIONS_H_

#include <exception>
#include <string>
#include <utility>

class WorkflowException : public std::exception {
public:
	explicit WorkflowException(std::string err) : err_(std::move(err)) {}
	const char *what() const noexcept override {
		return err_.c_str();
	}
private:
	std::string err_;
};

#endif //TASK_2_SRC_WORKFLOWEXCEPTIONS_H_
