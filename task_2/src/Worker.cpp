#include <stdexcept>
#include "IWorker.h"
#include "WorkflowExceptions.h"

#include <algorithm>
#include <fstream>
#include <string>

Arguments::Arguments(const std::string &str) {
	std::string new_arg;
	for (auto symb : str) {
		if (symb != ' ') {
			new_arg += symb;
		} else if (!new_arg.empty()) {
			args_.push_back(new_arg);
			new_arg.clear();
		}
	}
	if (!new_arg.empty()) {
		args_.push_back(new_arg);
	}
}
std::string Arguments::getArgument() {
	if (index_ < args_.size()) {
		return args_[index_++];
	}
	return std::string("");
}
int Arguments::size() {
	return args_.size();
}

WorkerMetaModel::WorkerMetaModel(const std::string &line) {
	auto equal = line.find(" = ");
	if (equal == std::string::npos) {
		throw WorkflowException("Didn't find '=' ");
	}
	try {
		id_ = std::stoi(line.substr(0, equal));
	} catch (std::invalid_argument &) {
		throw WorkflowException("Bad id");
	} catch (std::out_of_range &) {
		throw WorkflowException("Big id");
	}
	std::string block;
	std::string tail = line.substr(equal + 3);
	for (auto it = tail.begin(); it != tail.end() && *it != ' '; ++it) {
		block += *it;
	}
	if (block == "readfile") {
		type_ = WorkerType::READFILE;
	} else if (block == "writefile") {
		type_ = WorkerType::WRITEFILE;
	} else if (block == "grep") {
		type_ = WorkerType::GREP;
	} else if (block == "sort") {
		type_ = WorkerType::SORT;
	} else if (block == "replace") {
		type_ = WorkerType::REPLACE;
	} else if (block == "dump") {
		type_ = WorkerType::DUMP;
	} else {
		throw WorkflowException("Invalid worker name");
	}
	args_ = Arguments(line.substr(equal + 3 + block.length()));
}
int WorkerMetaModel::getId() {
	return id_;
}
WorkerType WorkerMetaModel::getType() {
	return type_;
}
Arguments WorkerMetaModel::getArgs() {
	return args_;
}
Worker::Worker(const Arguments &arg) : args_(arg) {}

WorkerType ReadFileWorker::type() {
	return READFILE;
}

WorkerType WriteFileWorker::type() {
	return WRITEFILE;
}
WorkerType GrepWorker::type() {
	return GREP;
}
WorkerType SortWorker::type() {
	return SORT;
}
WorkerType ReplaceWorker::type() {
	return REPLACE;
}
WorkerType DumpWorker::type() {
	return DUMP;
}

std::string ReadFileWorker::execute(std::string &string) {
	std::string file_name = args_.getArgument();
	std::ifstream in;
	in.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		in.open(file_name);
	} catch (std::ifstream::failure &) {
		throw WorkflowException("Readfile err. I can't open '" + file_name + "'");
	}
	in.seekg(0, in.end);
	int size = in.tellg();
	in.seekg(0, in.beg);

	std::string str(size, '\0');
	in.read(&str[0], size);
	in.close();
	return str;
}

std::string WriteFileWorker::execute(std::string &text) {
	std::string file_name = args_.getArgument();
	std::ofstream out;
	out.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		out.open(file_name);
	} catch (std::ifstream::failure &) {
		throw WorkflowException("Write err. I can't open '" + file_name + "'");
	}
	out << text;
	out.close();
	return std::string("");
}

std::string GrepWorker::execute(std::string &text) {
	std::string word = args_.getArgument();
	std::string line;
	std::string result;

	for (char symbol : text) {
		if (symbol != '\n') {
			line += symbol;
		} else if (line.find(word) != std::string::npos) {
			result += line + '\n';
			line.clear();
		} else {
			line.clear();
		}
	}
	if (line.find(word) != std::string::npos) {
		result += line;
	}
	return result;
}

std::string SortWorker::execute(std::string &text) {
	std::string line;
	std::vector<std::string> v;
	for (char symbol : text) {
		if (symbol != '\n') {
			line += symbol;
		} else {
			v.push_back(line);
			line.clear();
		}
	}
	if (!line.empty()) {
		v.push_back(line);
	}
	std::sort(v.begin(), v.end());
	std::string result;
	for (const auto &i : v) {
		result += i + '\n';
	}
	return result;
}

std::string ReplaceWorker::execute(std::string &text) {
	std::string word1 = args_.getArgument();
	if (word1.empty()) {
		return text;
	}
	std::string word2 = args_.getArgument();
	for (auto position = text.find(word1); position != std::string::npos; position = text.find(word1, position)) {
		text.replace(position, word1.length(), word2);
		position += word2.length();
	}
	return text;
}

std::string DumpWorker::execute(std::string &text) {
	std::string file_name = args_.getArgument();
	std::ofstream out;
	out.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	try {
		out.open(file_name);
	} catch (std::ifstream::failure &) {
		throw WorkflowException("Dump err. I can't opened '" + file_name + "'");
	}
	out << text;
	out.close();
	return text;
}
