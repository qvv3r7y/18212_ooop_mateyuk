#include <iostream>
#include <cstring>

#include "IParser.h"
#include "Scheme.h"
#include "WorkflowExceptions.h"


int main(int argc, char **argv) {
	if (argc < 2) {
		std::cerr << "Enter ./workflow filename.txt" << std::endl;
		return 1;
	}
	Parser parser;
	Scheme scheme;
	std::string input_filename;
	std::string output_filename;

	if (argc != 2) {
		for (int i = 1; i < argc; ++i) {
			if (!strcmp(argv[i], "-i")) {
				input_filename = argv[i + 1];
			} else if (!strcmp(argv[i], "-o")) {
				output_filename = argv[i + 1];
			}
		}
	}

	try {
		scheme = parser.parse(std::string(argv[1]));
	} catch (WorkflowException &exception) {
		std::cerr << exception.what() << std::endl;
		return 1;
	}

	try {
		scheme.control(input_filename, output_filename);
	} catch (WorkflowException &exception) {
		std::cerr << exception.what() << std::endl;
		return 1;
	}
	return 0;
}
