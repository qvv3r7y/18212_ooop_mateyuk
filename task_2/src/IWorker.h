#ifndef TASK_2_SRC_IWORKER_H_
#define TASK_2_SRC_IWORKER_H_

#include <string>
#include <vector>

enum WorkerType {
	READFILE = 0,
	WRITEFILE,
	GREP,
	SORT,
	REPLACE,
	DUMP
};

class Arguments {
public:
	Arguments() = default;
	Arguments(const Arguments &) = default;
	explicit Arguments(const std::string &);
	std::string getArgument();
	int size();
private:
	size_t index_ = 0;
	std::vector<std::string> args_;
};

class WorkerMetaModel {
public:
	explicit WorkerMetaModel(const std::string &);
	int getId();
	WorkerType getType();
	Arguments getArgs();
private:
	int id_;
	WorkerType type_;
	Arguments args_;
};

class IWorker {
public:
	virtual std::string execute(std::string &) = 0;
	virtual WorkerType type() = 0;
	virtual ~IWorker() = default;
};

class Worker : public IWorker {
public:
	Worker() = default;
	//~Worker() override = default;
	explicit Worker(const Arguments &);
protected:
	Arguments args_;
};

class ReadFileWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

class WriteFileWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

class GrepWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

class SortWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

class ReplaceWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

class DumpWorker : public Worker {
public:
	using Worker::Worker;
	std::string execute(std::string &) override;
	WorkerType type() override;
};

#endif //TASK_2_SRC_IWORKER_H_
