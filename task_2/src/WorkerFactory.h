#ifndef TASK_2_SRC_WORKERFACTORY_H_
#define TASK_2_SRC_WORKERFACTORY_H_

#include <memory>
#include <map>
#include "IWorker.h"

class IFactoryCreate {
public:
	virtual std::shared_ptr<IWorker> create(const Arguments &) = 0;
	virtual ~IFactoryCreate() = default;
};

template<class T>
class WorkerCreater : public IFactoryCreate {
	std::shared_ptr<IWorker> create(const Arguments &arguments) override {
		return std::make_shared<T>(arguments);
	}
	//~WorkerCreater() override = default;
};

class WorkerFactory {
public:
	WorkerFactory();
	~WorkerFactory();
	std::shared_ptr<IWorker> createWorker(WorkerType, const Arguments &) const;
private:
	std::map<WorkerType, IFactoryCreate *> creators_;
};

#endif //TASK_2_SRC_WORKERFACTORY_H_
