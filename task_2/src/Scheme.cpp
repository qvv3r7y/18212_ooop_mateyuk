#include "Scheme.h"

Scheme::Scheme(std::map<int, std::shared_ptr<IWorker>> &blocks, std::vector<int> &scheme)
	: blocks_(blocks), scheme_(scheme) {}

std::map<int, std::shared_ptr<IWorker>> Scheme::getBlocks() {
	return blocks_;
}
std::vector<int> Scheme::getScheme() {
	return scheme_;
}

void Scheme::process(std::string &buffer) {
	int prev_id = -1;
	for (size_t i = 0; i < scheme_.size(); ++i) {
		int id = scheme_[i];
		if (id < 0) {
			throw WorkflowException("Invalid ID");
		}

		if (i > 1 && (blocks_[prev_id]->type() == READFILE || blocks_[prev_id]->type() == WRITEFILE)) {
			throw WorkflowException("Readfile or writefile in the middle of the conveyor");
		}
		if (blocks_.find(id) == blocks_.end()) {
			throw WorkflowException("ID does not exist");
		}
		try {
			buffer = blocks_[id]->execute(buffer);
		} catch (WorkflowException &exception) {
			throw WorkflowException(exception.what());
		}
		prev_id = id;
	}
}
void Scheme::control(const std::string &input_filename, const std::string &output_filename) {
	WorkerFactory factory;
	std::string buffer;
	size_t size = scheme_.size();
	if (blocks_.find(scheme_[0]) == blocks_.end()) {
		throw WorkflowException("ID does't exist");
	}
	if (blocks_.find(scheme_[size - 1]) == blocks_.end()) {
		throw WorkflowException("ID does not exist");
	}
	if (blocks_[scheme_[0]]->type() != WorkerType::READFILE && input_filename.empty()) {
		throw WorkflowException("Incorrect first block");
	} else if (blocks_[scheme_[0]]->type() != WorkerType::READFILE && !input_filename.empty()) {
		auto readFile = factory.createWorker(READFILE, Arguments(input_filename));
		buffer = readFile->execute(buffer);
	}

	process(buffer);

	if (blocks_[scheme_[size - 1]]->type() != WorkerType::WRITEFILE && output_filename.empty()) {
		throw WorkflowException("Incorrect last block");
	} else if (blocks_[scheme_[size - 1]]->type() != WorkerType::WRITEFILE && !output_filename.empty()) {
		auto writeFile = factory.createWorker(WRITEFILE, Arguments(output_filename));
		writeFile->execute(buffer);
	}
}
