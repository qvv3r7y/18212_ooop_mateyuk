#include "WorkerFactory.h"

WorkerFactory::WorkerFactory() {
	creators_[READFILE] = new WorkerCreater<ReadFileWorker>;
	creators_[WRITEFILE] = new WorkerCreater<WriteFileWorker>;
	creators_[GREP] = new WorkerCreater<GrepWorker>;
	creators_[SORT] = new WorkerCreater<SortWorker>;
	creators_[REPLACE] = new WorkerCreater<ReplaceWorker>;
	creators_[DUMP] = new WorkerCreater<DumpWorker>;
}
WorkerFactory::~WorkerFactory() {
	for (auto it : creators_) {
		delete it.second;
		it.second = nullptr;
	}

}

std::shared_ptr<IWorker> WorkerFactory::createWorker(WorkerType type, const Arguments &args) const {
	auto it = creators_.find(type);
	if (it == creators_.end()) return nullptr;
	return it->second->create(args);
}