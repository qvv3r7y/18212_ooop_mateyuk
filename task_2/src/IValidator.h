#ifndef TASK_2_SRC_IVALIDATOR_H_
#define TASK_2_SRC_IVALIDATOR_H_

#include "IWorker.h"

class IValidator {
public:
	virtual bool isExist(WorkerMetaModel model) = 0;
	virtual bool isValid(WorkerMetaModel model) = 0;
	virtual ~IValidator() = default;
};

class ReadFileValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};


class WriteFileValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};

class GrepValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};

class SortValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};

class ReplaceValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};

class DumpValidator : public IValidator {
	bool isValid(WorkerMetaModel model) override;
	bool isExist(WorkerMetaModel model) override;
};

#endif //TASK_2_SRC_IVALIDATOR_H_
