#include "IValidator.h"

bool ReadFileValidator::isExist(WorkerMetaModel model) {
	return model.getType() == READFILE;
}
bool ReadFileValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 1;
}

bool WriteFileValidator::isExist(WorkerMetaModel model) {
	return model.getType() == WRITEFILE;
}
bool WriteFileValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 1;
}

bool GrepValidator::isExist(WorkerMetaModel model) {
	return model.getType() == GREP;
}
bool GrepValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 1;
}

bool SortValidator::isExist(WorkerMetaModel model) {
	return model.getType() == SORT;
}
bool SortValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 0;
}

bool ReplaceValidator::isExist(WorkerMetaModel model) {
	return model.getType() == REPLACE;
}
bool ReplaceValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 2;
}

bool DumpValidator::isExist(WorkerMetaModel model) {
	return model.getType() == DUMP;
}
bool DumpValidator::isValid(WorkerMetaModel model) {
	return model.getArgs().size() == 1;
}