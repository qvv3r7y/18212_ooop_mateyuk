#include <gtest/gtest.h>
#include "../src/IParser.h"

#include <iostream>
#include <fstream>

namespace {


std::string read_all_file(const std::string &filename) {
	std::ifstream fin(filename);
	fin.seekg(0, fin.end);
	int size = fin.tellg();
	fin.seekg(0, fin.beg);

	std::string s(size, '\0');

	fin.read(&s[0], size); // read all text
	fin.close();
	return s;
}

TEST(Arguments, simple_test) {
	Arguments test("1 2 3");
	for (int i = 1; i <= 3; ++i) {
		std::string h = test.getArgument();
		EXPECT_EQ(std::to_string(i), h);
	}
	EXPECT_EQ("", test.getArgument());
}

TEST(Arguments, space_end) {
	Arguments test("1 2 3      ");
	for (int i = 1; i <= 3; ++i) {
		std::string h = test.getArgument();
		EXPECT_EQ(std::to_string(i), h);
	}
	EXPECT_EQ("", test.getArgument());
}

TEST(Arguments, space_begin) {
	Arguments test("       1 2 3");
	for (int i = 1; i <= 3; ++i) {
		std::string h = test.getArgument();
		EXPECT_EQ(std::to_string(i), h);
	}
	EXPECT_EQ("", test.getArgument());
}

TEST(Arguments, default_value) {
	Arguments test;
	EXPECT_EQ("", test.getArgument());
}

TEST(Arguments, many_spaces) {
	Arguments test("     word1    word2    word3     word4      ");
	for (int i = 1; i <= 4; ++i) {
		std::string h = test.getArgument();
		EXPECT_EQ("word" + std::to_string(i), h);
	}
	EXPECT_EQ("", test.getArgument());
}

TEST(FileReaderWorker, read) {
	std::ofstream fout("test_file.txt");
	fout << "abcd\n"
	        "abba\n"
	        "1234567890";
	fout.close();

	Arguments args("test_file.txt");
	ReadFileWorker block(args);
	std::string read_text = block.execute((std::string &) "");
	EXPECT_EQ("abcd\n"
	          "abba\n"
	          "1234567890",
	          read_text);
	std::remove("test_file.txt");
}

TEST(FileWriterWorker, write) {
	std::string text = "1234567890-=\n"
	                   "random_text____\n\n\t\n\n";

	WriteFileWorker block(Arguments("test_file.txt"));
	block.execute(text);

	std::string s = read_all_file("test_file.txt");
	std::remove("test_file.txt");

	EXPECT_EQ("1234567890-=\n"
	          "random_text____\n\n\t\n\n",
	          s);
}

TEST(GrepWorker, simple_test) {
	GrepWorker block(Arguments("123"));
	std::string text = "123\n"
	                   "456\n"
	                   "123\n"
	                   "789\n"
	                   "text 123 text\n"
	                   "123 text\n"
	                   "text 123\n";
	std::string result = block.execute(text);
	EXPECT_EQ("123\n"
	          "123\n"
	          "text 123 text\n"
	          "123 text\n"
	          "text 123\n",
	          result);
}

TEST(GrepWorker, last_line) {
	GrepWorker block(Arguments("FIND"));
	std::string text = "FIND\n"
	                   "456\n"
	                   "FINDFINDFINDFIND\n"
	                   "789\n"
	                   "text 123 text\n"
	                   "123 text\n"
	                   "FIND";
	std::string result = block.execute(text);
	EXPECT_EQ("FIND\n"
	          "FINDFINDFINDFIND\n"
	          "FIND",
	          result);
}

TEST(GrepWorker, empty_word) {
	GrepWorker block(Arguments(""));
	std::string text = "FIND\n"
	                   "456\n"
	                   "FINDFINDFINDFIND\n"
	                   "789\n"
	                   "text 123 text\n"
	                   "123 text\n"
	                   "FIND";
	std::string result = block.execute(text);
	EXPECT_EQ("FIND\n"
	          "456\n"
	          "FINDFINDFINDFIND\n"
	          "789\n"
	          "text 123 text\n"
	          "123 text\n"
	          "FIND",
	          result);
}

TEST(GrepWorker, empty_line) {
	GrepWorker block(Arguments("FIND"));
	std::string text = "\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n";
	std::string result = block.execute(text);
	EXPECT_EQ("FIND\n", result);
}

TEST(GrepWorker, empty_line_empty_word) {
	GrepWorker block(Arguments(""));
	std::string text = "\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n";
	std::string result = block.execute(text);
	EXPECT_EQ("\n\n\n\n\n\n\n\n\nFIND\n\n\n\n\n\n\n\n\n\n\n\n", result);
}

TEST(GrepWorker, empty_result) {
	GrepWorker block(Arguments("FIND"));
	std::string text = "qwerty\n"
	                   "random word\n"
	                   "hello world! =)";
	std::string result = block.execute(text);
	EXPECT_EQ("", result);
}

TEST(GrepWorker, register_symbol) {
	GrepWorker block(Arguments("fInD"));
	std::string text = "find\n"
	                   "Find\n"
	                   "fInd\n"
	                   "fiNd\n"
	                   "finD\n"
	                   "FInd\n"
	                   "FiNd\n"
	                   "FinD\n"
	                   "fINd\n"
	                   "fInD\n"
	                   "fiND\n"
	                   "FINd\n"
	                   "FInD\n"
	                   "FiND\n"
	                   "fIND\n"
	                   "FIND\n";
	std::string result = block.execute(text);
	EXPECT_EQ("fInD\n", result);
}

TEST(WorkerMetaModel, ReadFile) {
	WorkerMetaModel meta("0 = readfile in.txt");
	EXPECT_EQ(WorkerType::READFILE, meta.getType());
	EXPECT_EQ(0, meta.getId());
	ASSERT_EQ(1, meta.getArgs().size());
	EXPECT_EQ("in.txt", meta.getArgs().getArgument());
}

TEST(WorkerMetaModel, WriteFile) {
	WorkerMetaModel meta("5 = writefile out.txt");
	EXPECT_EQ(WorkerType::WRITEFILE, meta.getType());
	EXPECT_EQ(5, meta.getId());
	ASSERT_EQ(1, meta.getArgs().size());
	EXPECT_EQ("out.txt", meta.getArgs().getArgument());
}

TEST(WorkerMetaModel, Grep) {
	WorkerMetaModel meta("2 = grep braab");
	EXPECT_EQ(WorkerType::GREP, meta.getType());
	EXPECT_EQ(2, meta.getId());
	ASSERT_EQ(1, meta.getArgs().size());
	EXPECT_EQ("braab", meta.getArgs().getArgument());
}

TEST(WorkerMetaModel, Sort) {
	WorkerMetaModel meta("3 = sort");
	EXPECT_EQ(WorkerType::SORT, meta.getType());
	EXPECT_EQ(3, meta.getId());
	ASSERT_EQ(0, meta.getArgs().size());
	EXPECT_EQ("", meta.getArgs().getArgument());
}

TEST(WorkerMetaModel, Replace) {
	WorkerMetaModel meta("1 = replace abracadabra cadabraabra");
	EXPECT_EQ(WorkerType::REPLACE, meta.getType());
	EXPECT_EQ(1, meta.getId());
	Arguments args = meta.getArgs();
	ASSERT_EQ(2, args.size());
	EXPECT_EQ("abracadabra", args.getArgument());
	EXPECT_EQ("cadabraabra", args.getArgument());
	EXPECT_EQ("", args.getArgument());
}

TEST(WorkerMetaModel, Dump) {
	WorkerMetaModel meta("100500 = dump check.txt");
	EXPECT_EQ(WorkerType::DUMP, meta.getType());
	EXPECT_EQ(100500, meta.getId());
	ASSERT_EQ(1, meta.getArgs().size());
	EXPECT_EQ("check.txt", meta.getArgs().getArgument());
}

} //end empty namespace


int main(int argc, char **argv) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}

