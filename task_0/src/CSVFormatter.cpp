
#include "CSVFormatter.h"

std::string CSVFormatter::format(std::vector<std::string> data) {
	setDelim(',');
	std::string str;
	for (const auto &now : data) {
		str.append(now);
		str.append(1, delim);
		str.append(1, ' ');
	}
	str.pop_back();
	str.back() = '%';
	return str;
}
