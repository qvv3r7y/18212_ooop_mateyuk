
#ifndef TASK_0_FORMATSTRING_H
#define TASK_0_FORMATSTRING_H

#include <string>
#include <vector>

class FormatString {
public:
	FormatString();

	virtual std::string format(std::vector<std::string>) = 0;

	void setDelim(char);

protected:
	char delim;
};

#endif //TASK_0_FORMATSTRING_H
