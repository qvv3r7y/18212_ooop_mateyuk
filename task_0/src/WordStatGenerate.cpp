
#include "WordStatGenerate.h"
#include <algorithm>

bool compare(const std::vector<std::string> &a, const std::vector<std::string> &b) {
	return a.back() > b.back();
}

WordStatGenerate::WordStatGenerate() : count_words(0) {}

void WordStatGenerate::addStat(const std::vector<std::string> &new_words) {
	for (const auto &now : new_words) {
		if (now.empty()) continue;
		word_qty_freq[now].first++;
		count_words++;
	}
}

void WordStatGenerate::updateWordFreq() {
	for (auto &now : word_qty_freq) {
		now.second.second = now.second.first*100.0/count_words;
	}
}

std::vector<std::vector<std::string>> WordStatGenerate::getSortData() {
	std::vector<std::vector<std::string>> data;
	for (const auto &now : word_qty_freq) {
		std::vector<std::string> tmp_str;
		tmp_str.push_back(now.first);
		tmp_str.push_back(std::to_string(now.second.first));
		tmp_str.push_back(std::to_string(now.second.second));
		data.push_back(tmp_str);
	}
	std::sort(data.begin(), data.end(), compare);
	return data;
};
