
#ifndef TASK_0_CSVFORMATTER_H
#define TASK_0_CSVFORMATTER_H

#include "FormatString.h"

class CSVFormatter : public FormatString::FormatString {
public:
	CSVFormatter() : FormatString() {};

	std::string format(std::vector<std::string>) override;

};

#endif //TASK_0_CSVFORMATTER_H
