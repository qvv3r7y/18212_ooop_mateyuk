#ifndef TASK_0_READER_H
#define TASK_0_READER_H

#include <fstream>

class Reader {
public:
	explicit Reader(const std::string &);

	bool isOpen();

	bool isEof();

	std::string getLine();

	~Reader();

private:
	std::ifstream in_stream;
};

#endif //TASK_0_READER_H