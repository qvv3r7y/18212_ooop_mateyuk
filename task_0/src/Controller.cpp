
#include "Controller.h"
#include "Reader.h"
#include "Reporter.h"
#include "WordParser.h"
#include "WordStatGenerate.h"

void Controller::process(const std::string &in) {
	Reader file_read(in);
	if (!file_read.isOpen()){
		return;
	}
	WordParser word_parser;
	WordStatGenerate word_stat_generate;
	while (!file_read.isEof()) {
		std::string str = file_read.getLine();
		if (str.empty()) {
			continue;
		}
		std::vector<std::string> words = word_parser.parse(str);
		word_stat_generate.addStat(words);
	}
	word_stat_generate.updateWordFreq();
	Reporter reporter;
	reporter.report(word_stat_generate.getSortData());
}
