#include <utility>
#include "WordParser.h"

std::vector<std::string> WordParser::parse(const std::string &str) {
	std::vector<std::string> words;
	std::string new_word;
	for (auto symb: str) {
		if (delimiters.find(symb)==std::string::npos) {
			new_word.push_back(symb);
		} else {
			words.push_back(new_word);
			new_word.clear();
		}
	}
	words.push_back(new_word);
	return words;
}

void WordParser::setDelimiters(std::string str) {
	delimiters = std::move(str);
}
