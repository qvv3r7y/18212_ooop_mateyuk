
#ifndef TASK_0_WORDPARSER_H
#define TASK_0_WORDPARSER_H

#include <string>
#include <vector>

class WordParser {
public:
	std::vector<std::string> parse(const std::string &);

	void setDelimiters(std::string);

private:
	std::string delimiters = "“” ,.!?/\";:-*_><{}[]|()^%$#@=+–\n";
};

#endif //TASK_0_WORDPARSER_H
