#include <iostream>
#include "Controller.h"

int main(int argc, char **argv) {
	if (argc!=2) {
		std::cout << "\nUse .txt file with this program\n";
	} else {
		Controller::process(argv[1]);
	}
	return 0;
}
