
#ifndef TASK_0_WORDSTATGENERATE_H
#define TASK_0_WORDSTATGENERATE_H

#include <map>
#include <vector>

class WordStatGenerate {
public:
	WordStatGenerate();

	void addStat(const std::vector<std::string> &new_words);

	void updateWordFreq();

	std::vector<std::vector<std::string>> getSortData();

private:
	size_t count_words;
	std::map<std::string, std::pair<size_t, double >> word_qty_freq;
};

#endif //TASK_0_WORDSTATGENERATE_H
