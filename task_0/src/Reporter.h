
#ifndef TASK_0_REPORTER_H
#define TASK_0_REPORTER_H

#include "CSVFormatter.h"
#include <vector>

class Reporter {
public:
	void report(const std::vector<std::vector<std::string>> &);

private:
	CSVFormatter csv;
};

#endif //TASK_0_REPORTER_H
