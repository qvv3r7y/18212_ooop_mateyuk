
#ifndef TASK_0_CONTROLLER_H
#define TASK_0_CONTROLLER_H

#include <string>

class Controller {
public:
	static void process(const std::string &);
};

#endif //TASK_0_CONTROLLER_H
