#include "Reader.h"
#include <iostream>

Reader::Reader(const std::string &file) {
	in_stream.open(file, std::ios::in);
}
bool Reader::isOpen(){
	if (!in_stream.is_open()) {
		std::cout << "Wrong path\n";
		return false;
	}
	return true;
}

Reader::~Reader() { in_stream.close(); }

std::string Reader::getLine() {
	std::string str;
	std::getline(this->in_stream, str);
	return str;
}

bool Reader::isEof() { return this->in_stream.eof(); }
