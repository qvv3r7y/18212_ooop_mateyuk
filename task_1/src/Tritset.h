#ifndef TASK_1_SRC_TRITSET_H_
#define TASK_1_SRC_TRITSET_H_

#include <cstdint>
#include <cstring>
#include <unordered_map>

namespace tritset {

class Tritset {
public:

	explicit Tritset(std::size_t capacity);
	Tritset(const Tritset &tritset);
	Tritset &operator=(const Tritset &tritset);
	~Tritset();
	Tritset(Tritset &&tritset) noexcept;
	Tritset &operator=(Tritset &&tritset) noexcept;

	enum TritValue {
		FALSE = -1,
		UNKNOWN,
		TRUE
	};

	class Trit {
	public:
		Trit(Tritset *tritset, size_t trit_index);
		Trit(const Trit &trit) = default;
		Trit(Trit &&trit) = default;
		Trit &operator=(Trit &&trit) = default;
		Trit &operator=(TritValue value);

		operator TritValue() const;
		bool operator==(TritValue value) const;
		bool operator!=(TritValue value) const;
		TritValue getValue() const;
	private:
		void setValue(TritValue value);

		size_t trit_index_;
		Tritset *tritset_;
	};

	void shrink();
	void trim(size_t last_index);
	size_t capacity() const;
	size_t logicalLength();
	size_t cardinality(TritValue value);
	std::unordered_map<TritValue, int, std::hash<int> > cardinality();

	Trit operator[](size_t trit_index);
	Tritset &operator!();
	Tritset &operator&=(Tritset &tritset);
	Tritset &operator|=(Tritset &tritset);


private:
	std::size_t size_;                /// QTY UINT
	std::uint32_t *trits_;
	std::size_t last_set_bit_;        /// FOR SHRINK
};

Tritset operator|(Tritset left, Tritset &right);
Tritset operator&(Tritset left, Tritset &right);

} // namespace

#endif //TASK_1_SRC_TRITSET_H_
