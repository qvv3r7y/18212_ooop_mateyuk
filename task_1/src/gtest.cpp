#include "Tritset.h"
#include <gtest/gtest.h>

using namespace tritset;

constexpr auto UNKNOWN =  Tritset::UNKNOWN;
constexpr auto FALSE =  Tritset::FALSE;
constexpr auto TRUE =  Tritset::TRUE;

namespace {

TEST(InitTritset, GetSetValue) {
	Tritset set(128);
	set[0] = TRUE;
	set[127] = FALSE;

	size_t alloc_len = set.capacity();
	set[128] = UNKNOWN;
	ASSERT_EQ(alloc_len, set.capacity());

	ASSERT_EQ(set[0].getValue(), TRUE);
	ASSERT_EQ(set[127].getValue(), FALSE);
	ASSERT_EQ(set[1000].getValue(), UNKNOWN);

	ASSERT_EQ(alloc_len, set.capacity());
}

TEST(AllocMemory, TritsetLiveCycle) {
	Tritset set0(std::move(Tritset(1000)));
	Tritset set(0);
	set = std::move(set0);

	size_t alloc_len = set.capacity();
	EXPECT_GE(alloc_len, 1000 * 2 / 8 / sizeof(uint32_t));

	//не выделяет никакой памяти
	set[1000000000] = UNKNOWN;
	EXPECT_EQ(alloc_len, set.capacity());

	// false, but no memory allocation
	EXPECT_EQ(set[2000000000] == TRUE, false);
	EXPECT_EQ(alloc_len, set.capacity());

	//выделение памяти
	set[1000000000] = TRUE;
	EXPECT_LT(alloc_len, set.capacity());

	//no memory operations
	alloc_len = set.capacity();
	set[1000000000] = UNKNOWN;
	set[1000000] = FALSE;
	EXPECT_EQ(alloc_len, set.capacity());

	//освобождение памяти
	//до значения необходимого для хранения последнего установленного трита
	//в данном случае для трита 1'000'000
	set.shrink();
	EXPECT_GT(alloc_len, set.capacity());
}

TEST(BoolFunc, EqNeq) {
	Tritset set(10);
	set[0] = TRUE;
	set[10] = FALSE;
	EXPECT_EQ(set[0] == set[10], false);
	EXPECT_EQ(set[10] == set[60], false);
	EXPECT_EQ(set[5] == set[11], true);
	EXPECT_EQ(set[10] == set[10], true);

}

TEST(BitOperation, NOT) {
	Tritset setA(100);

	setA[123] = TRUE;
	setA[124] = FALSE;
	setA[125] = UNKNOWN;

	Tritset setC = !setA;

	EXPECT_EQ(setA[123] == FALSE, true);
	EXPECT_EQ(setC[124] == TRUE, true);
	EXPECT_EQ(setA[125] == UNKNOWN, true);

	EXPECT_EQ(setA.capacity(), setC.capacity());
}

TEST(BitOperation, OR) {
	Tritset setA(100);
	Tritset setB(200);

	setA[123] = TRUE;
	setA[124] = FALSE;
	setA[125] = UNKNOWN;

	setB[123] = FALSE;
	setB[124] = FALSE;
	setB[125] = FALSE;

	setA |= setB;

	EXPECT_EQ(setA[123] == TRUE, true);
	EXPECT_EQ(setA[124] == FALSE, true);
	EXPECT_EQ(setA[125] == UNKNOWN, true);

	Tritset setC = setA | setB;
	EXPECT_EQ(setB.capacity(), setC.capacity());
}

TEST(BitOperation, AND) {
	Tritset setA(150);
	Tritset setB(2000);

	setA[123] = TRUE;
	setA[124] = TRUE;
	setA[125] = UNKNOWN;

	setB[123] = FALSE;
	setB[124] = TRUE;
	setB[125] = TRUE;

	setA &= setB;

	EXPECT_EQ(setA[123] == FALSE, true);
	EXPECT_EQ(setA[124] == TRUE, true);
	EXPECT_EQ(setA[125] == UNKNOWN, true);

	Tritset setC = setA & setB;
	EXPECT_EQ(setB.capacity(), setC.capacity());
}

TEST(InitTritset, CheckConstr) {
	Tritset set(1000);
	Tritset set1(0);
	Tritset set2(1000000000);

	size_t alloc_len = set.capacity();
	EXPECT_GE(alloc_len, 1000 * 2 / 8 / sizeof(uint32_t));

	alloc_len = set1.capacity();
	EXPECT_GE(alloc_len, 0);

	alloc_len = set2.capacity();
	EXPECT_GE(alloc_len, 1000000000 * 2 / 8 / sizeof(uint32_t));
}

TEST(TritsetMethods, LogicalLength) {
	Tritset set(100);
	EXPECT_EQ(set.logicalLength(), 0);

	set[30] = TRUE;
	EXPECT_EQ(set.logicalLength(), 30);

	set[20] = FALSE;
	EXPECT_EQ(set.logicalLength(), 30);

	set[100] = UNKNOWN;
	EXPECT_EQ(set.logicalLength(), 30);
}

TEST(TritsetMethods, Trim) {
	Tritset set(100);
	EXPECT_EQ(set.logicalLength(), 0);

	set[30] = TRUE;
	set[19] = FALSE;
	set[20] = FALSE;
	set.trim(20);
	EXPECT_EQ(set.logicalLength(), 19);

}

TEST(TritsetMethods, Shrink) {
	Tritset set(100);
	set.shrink();
	EXPECT_EQ(set.capacity(), 16);

	for (int i = 0; i < 100; ++i) {
		set[i] = FALSE;
	}

	set[30] = TRUE;
	set.shrink();
	EXPECT_EQ(set.capacity(), 32);

	set[15] = FALSE;
	set.shrink();
	EXPECT_EQ(set.capacity(), 16);

	set[100] = UNKNOWN;
	set.shrink();
	EXPECT_EQ(set.capacity(), 16);
}

TEST(TritsetMethods, CardinalityValue) {
	Tritset set(99);
	for (int i = 0; i < 93; i += 3) {
		set[i] = TRUE;
		set[i + 1] = FALSE;
		set[i + 2] = UNKNOWN;
	}
	set[2] = TRUE;

	EXPECT_EQ(set.cardinality(TRUE), 32);
	EXPECT_EQ(set.cardinality(FALSE), 31);
	EXPECT_EQ(set.cardinality(UNKNOWN), 29);
}

TEST(TritsetMethods, CardinalityMap) {
	Tritset set(99);
	for (int i = 0; i < 93; i += 3) {
		set[i] = TRUE;
		set[i + 1] = FALSE;
		set[i + 2] = UNKNOWN;
	}
	set[2] = TRUE;

	std::unordered_map<Tritset::TritValue, int, std::hash<int>> value_set = set.cardinality();

	EXPECT_EQ(value_set[TRUE], 32);
	EXPECT_EQ(value_set[FALSE], 31);
	EXPECT_EQ(value_set[UNKNOWN], 29);
}

} /// end namespace

int main(int argc, char **argv) {
	testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
