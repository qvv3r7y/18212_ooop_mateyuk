#include <cmath>
#include "Tritset.h"
#include <iostream>

namespace tritset {

#define CVT_UINT_TRIT (sizeof(uint32_t) * 8 / 2)

Tritset::Tritset(std::size_t capacity) {
	size_ = ceil(capacity * 2.0 / (8 * sizeof(uint32_t)));
	trits_ = new uint32_t[size_]();
	last_set_bit_ = 0;
}

Tritset::~Tritset() {
	delete[] trits_;
}

Tritset &Tritset::operator=(const Tritset &tritset) {
	if (&tritset == this) {
		return *this;
	}
	last_set_bit_ = tritset.last_set_bit_;
	delete[] trits_;
	size_ = tritset.size_;
	trits_ = new uint32_t[size_]();
	memcpy(trits_, tritset.trits_, size_ * sizeof(uint32_t));
	return *this;
}

Tritset::Tritset(const Tritset &tritset)
	: size_(tritset.size_), trits_(new uint32_t[size_]()), last_set_bit_(tritset.last_set_bit_) {
	memcpy(trits_, tritset.trits_, size_ * sizeof(uint32_t));
}

Tritset::Trit Tritset::operator[](size_t trit_index) {
	return Trit(this, trit_index);
}

Tritset &Tritset::operator&=(Tritset &tritset) {
	if (this->size_ < tritset.size_) {
		auto *new_trits = new uint32_t[tritset.size_]();
		memcpy(new_trits, trits_, size_ * sizeof(uint32_t));
		delete[] trits_;
		size_ = tritset.size_;
		trits_ = new_trits;
		last_set_bit_ = tritset.last_set_bit_;
	}
	for (size_t i = 0; i < size_ * CVT_UINT_TRIT; ++i) {
		TritValue val_this = (*this)[i].getValue();
		TritValue val_that = tritset[i].getValue();
		if (val_this == FALSE || val_that == FALSE) (*this)[i] = FALSE;
		else if (val_this == UNKNOWN || val_that == UNKNOWN) (*this)[i] = UNKNOWN;
		else (*this)[i] = TRUE;
	}
	return *this;
}

Tritset &Tritset::operator|=(Tritset &tritset) {
	if (this->size_ < tritset.size_) {
		auto *new_trits = new uint32_t[tritset.size_]();
		memcpy(new_trits, trits_, size_ * sizeof(uint32_t));
		delete[] trits_;
		size_ = tritset.size_;
		trits_ = new_trits;
		last_set_bit_ = tritset.last_set_bit_;
	}
	for (size_t i = 0; i < size_ * CVT_UINT_TRIT; ++i) {
		TritValue val_this = (*this)[i].getValue();
		TritValue val_that = tritset[i].getValue();
		if (val_this == TRUE || val_that == TRUE) (*this)[i] = TRUE;
		else if (val_this == UNKNOWN || val_that == UNKNOWN) (*this)[i] = UNKNOWN;
		else (*this)[i] = FALSE;
	}
	return *this;
}

Tritset &Tritset::operator!() {
	for (size_t i = 0; i < size_ * CVT_UINT_TRIT; ++i) {
		if ((*this)[i] == FALSE) (*this)[i] = TRUE;
		else if ((*this)[i] == TRUE) (*this)[i] = FALSE;
	}
	return *this;
}

Tritset operator|(Tritset left, Tritset &right) {
	return left |= right;
}

Tritset operator&(Tritset left, Tritset &right) {
	return left &= right;
}
void Tritset::shrink() {
	size_ = last_set_bit_ * 2 / (8 * sizeof(uint32_t)) + 1;
	auto *new_trits = new uint32_t[size_]();
	memcpy(new_trits, trits_, size_ * sizeof(uint32_t));
	delete[] trits_;
	trits_ = new_trits;
}

size_t Tritset::logicalLength() {
	size_t i = size_ * CVT_UINT_TRIT - 1;
	while (true) {
		if ((*this)[i] != UNKNOWN) break;
		if (i == 0) break;
		--i;
	}
	return i;
}

size_t Tritset::cardinality(TritValue value) {
	size_t cardinality = 0;
	size_t i = logicalLength();
	while (true) {
		if ((*this)[i] == value) cardinality++;
		if (i == 0) break;
		--i;
	}
	return cardinality;
}

std::unordered_map<Tritset::TritValue, int, std::hash<int>> Tritset::cardinality() {
	std::unordered_map<TritValue, int, std::hash<int>> value_set(3);
	size_t i = logicalLength();
	while (true) {
		value_set[(*this)[i].getValue()]++;
		if (i == 0) break;
		--i;
	}
	return value_set;
}

void Tritset::trim(size_t last_index) {
	if (last_index >= this->size_ * CVT_UINT_TRIT) return;
	size_t erase = last_index;
	while (erase % CVT_UINT_TRIT) {
		(*this)[erase] = UNKNOWN;
		erase++;
	}
	size_t qty_erase_byte = size_ * sizeof(uint32_t) - (erase * 2 / 8);
	memset(&this->trits_[erase * 2 / (sizeof(uint32_t) * 8)], 0, qty_erase_byte);
}
size_t Tritset::capacity() const {
	return size_ * CVT_UINT_TRIT;
}

Tritset::Tritset(Tritset &&tritset) noexcept
	: size_(tritset.size_), trits_(tritset.trits_), last_set_bit_(tritset.last_set_bit_) {
	tritset.size_ = 0;
	tritset.last_set_bit_ = 0;
	tritset.trits_ = nullptr;
}
Tritset &Tritset::operator=(Tritset &&tritset) noexcept {
	if (&tritset == this) return *this;
	delete[] trits_;
	trits_ = tritset.trits_;
	size_ = tritset.size_;
	last_set_bit_ = tritset.last_set_bit_;

	tritset.size_ = 0;
	tritset.last_set_bit_ = 0;
	tritset.trits_ = nullptr;
	return *this;
}

Tritset::Trit::Trit(Tritset *tritset, size_t trit_index) : trit_index_(trit_index), tritset_(tritset) {}

Tritset::TritValue Tritset::Trit::getValue() const {
	if (tritset_->size_ * CVT_UINT_TRIT <= trit_index_) return UNKNOWN;
	size_t uint_ind = trit_index_ * 2 / (sizeof(uint32_t) * 8);
	size_t trit_begin = trit_index_ * 2 % (sizeof(uint32_t) * 8);
	uint32_t box = tritset_->trits_[uint_ind];
	uint32_t trit = (box >> (sizeof(uint32_t) * 8 - trit_begin - 2)) & 0b11u;
	switch (trit) {
		case 0b00:
		case 0b01:
			return UNKNOWN;
		case 0b10:
			return FALSE;
		case 0b11:
			return TRUE;
		default:                                        /// UNREACHABLE CODE FOR CLION
			std::cerr << "ERROR OF CONVERT TRIT\n";
			return UNKNOWN;
	}
}

void Tritset::Trit::setValue(TritValue value) {
	size_t uint_ind = trit_index_ * 2 / (sizeof(uint32_t) * 8);
	size_t trit_begin = trit_index_ * 2 % (sizeof(uint32_t) * 8);
	uint32_t box = tritset_->trits_[uint_ind];
	///for invert set/delete bit
	size_t fir_bit = (sizeof(uint32_t) * 8 - trit_begin - 1);
	size_t sec_bit = (sizeof(uint32_t) * 8 - trit_begin - 2);

	if (value == UNKNOWN) {
		box &= ~(1u << fir_bit);
	} else if (value == FALSE) {
		box |= (1u << fir_bit);
		box &= ~(1u << sec_bit);
	} else if (value == TRUE) {
		box |= (0b11u << sec_bit);
	}

	tritset_->trits_[uint_ind] = box;
	if (value != UNKNOWN) tritset_->last_set_bit_ = trit_index_;
}

Tritset::Trit &Tritset::Trit::operator=(TritValue value) {
	if (tritset_->size_ * CVT_UINT_TRIT <= trit_index_) {
		if (value == UNKNOWN) return *this;
		size_t new_size = trit_index_ * 2 / (8 * sizeof(uint32_t)) + 1;
		auto *new_trits = new uint32_t[new_size]();
		memcpy(new_trits, tritset_->trits_, tritset_->size_ * sizeof(uint32_t));
		delete[] tritset_->trits_;
		tritset_->size_ = new_size;
		tritset_->trits_ = new_trits;
		tritset_->last_set_bit_ = trit_index_;
	}
	this->setValue(value);
	return *this;
}

bool Tritset::Trit::operator==(TritValue value) const {
	if (tritset_->size_ * CVT_UINT_TRIT <= trit_index_) return false;
	return this->getValue() == value;
}

bool Tritset::Trit::operator!=(TritValue value) const {
	return !(operator==(value));
}

Tritset::Trit::operator TritValue() const {
	return this->getValue();
}

}