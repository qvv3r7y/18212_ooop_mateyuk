#ifndef SEABATTLE_SRC_SHIP_H_
#define SEABATTLE_SRC_SHIP_H_

#include <utility>
#include <cstdlib>

enum ShipType {
	ONE = 1,
	TWO,
	THREE,
	FOUR
};

enum ShipDirection {
	HORIZONTAL = 0,
	VERTICAL
};

enum ShipStatus {
	WATERED = 0,
	INJURED,
	ALIVE,
};

class Ship {
private:
	ShipType type_;
	std::pair<size_t, size_t> location_;  /// column, row
	ShipDirection direction_;
	ShipStatus status_ = ALIVE;
	size_t damages_ = 0;
	friend class Board;
public:
	Ship(ShipType type, std::pair<size_t, size_t> location, ShipDirection direction) : type_{type},
	                                                                                   location_{std::move(location)},
	                                                                                   direction_{direction} {};
	~Ship() = default;
	ShipStatus getStatus() const;
	ShipType getType() const;
	std::pair<size_t, size_t> getLocation()const;
	size_t getDamages()const;
	ShipDirection getDirection()const;
	void setStatus(ShipStatus status);
};

#endif //SEABATTLE_SRC_SHIP_H_
