#include <iostream>
#include "Board.h"

Board::Board(size_t size) {
	size_ = size;
	/*area_ = new bool *[size];
	for (size_t i = 0; i < size; ++i) {
		area_[i] = new bool[size]();
	}*/

	Ship ship(FOUR, std::make_pair(1, 1), VERTICAL);
	ships_.push_back(ship);

	ship.type_ = THREE;
	ships_.push_back(ship);
	ships_.push_back(ship);

	ship.type_ = TWO;
	ships_.push_back(ship);
	ships_.push_back(ship);
	ships_.push_back(ship);

	ship.type_ = ONE;
	ships_.push_back(ship);
	ships_.push_back(ship);
	ships_.push_back(ship);
	ships_.push_back(ship);
}

Board::~Board() {
/*	for (size_t i = 0; i < size_; i++) {
		delete[] area_[i];
	}
	delete[]area_;*/
}
Ship Board::setHit(std::pair<size_t, size_t> hit) {
	Ship ship(ONE,std::make_pair(1,1),VERTICAL);

	area_[hit.second - 1][hit.first - 1] = true;
	for (Ship &now : ships_) {
		int row = now.location_.second;
		int column = now.location_.first;
		for (int i = 0; i < now.type_; ++i) {
			if (hit.second == row && hit.first == column) {
				now.damages_++;
				if (now.damages_ == now.type_) {
					now.setStatus(WATERED);
					ship = now;
				} else {
					now.setStatus(INJURED);
					ship = now;
				}
				return ship;
			}
			if (now.direction_ == HORIZONTAL) column++;
			else row++;
		}
	}
	return ship;
}
void Board::showShips() const {
	char board_tmpl[13][27] = {
		{"\t    A B C D E F G H J K \n"},
		{"\t    _ _ _ _ _ _ _ _ _ _ \n"},
		{"\t 1 | | | | | | | | | | |\n"},
		{"\t 2 | | | | | | | | | | |\n"},
		{"\t 3 | | | | | | | | | | |\n"},
		{"\t 4 | | | | | | | | | | |\n"},
		{"\t 5 | | | | | | | | | | |\n"},
		{"\t 6 | | | | | | | | | | |\n"},
		{"\t 7 | | | | | | | | | | |\n"},
		{"\t 8 | | | | | | | | | | |\n"},
		{"\t 9 | | | | | | | | | | |\n"},
		{"\t10 | | | | | | | | | | |\n"},
		{"\t   `````````````````````\n"}
	};

	std::cout << '\n' << '\n' << '\n';
	std::cout << board_tmpl[0];
	std::cout << board_tmpl[1];
	/// 5 + 2 /// 2 + 1 /// 2*i + 3 /// j + 1

	for (auto now : ships_) {
		int row = now.location_.second;
		int column = now.location_.first;
		if ((row + now.type_ > size_ + 1 && now.direction_ == VERTICAL)
			|| (column + now.type_ > size_ + 1 && now.direction_ == HORIZONTAL)) {
			board_tmpl[ROW(row)][COLUMN(column)] = '@';
			continue;
		}
		for (int i = 0; i < now.type_; ++i) {
			board_tmpl[ROW(row)][COLUMN(column)] = '#';
			if (now.direction_ == HORIZONTAL) column++;
			else row++;
		}
	}

	for (int i = 2; i < 12; ++i) {
		std::cout << board_tmpl[i];
	}

	std::cout << board_tmpl[12];
}

void Board::moveShip(int index, std::pair<size_t, size_t> place, ShipDirection direction){
	ships_[index].direction_ = direction;
	ships_[index].location_ = place;
}

void Board::moveShip(int index, MoveWay way, bool direction) {
	if (direction) {
		if (ships_[index].direction_ == HORIZONTAL) ships_[index].direction_ = VERTICAL;
		else ships_[index].direction_ = HORIZONTAL;
		return;
	}
	if ((way == UP && ships_[index].location_.second == 1)
		|| (way == DOWN && ships_[index].location_.second == 10)
		|| (way == LEFT && ships_[index].location_.first == 1)
		|| (way == RIGHT && ships_[index].location_.first == 10)) {
		return;
	}
	if (way == UP) ships_[index].location_.second += -1;
	else if (way == DOWN) ships_[index].location_.second += 1;
	else if (way == LEFT) ships_[index].location_.first += -1;
	else if (way == RIGHT) ships_[index].location_.first += 1;
}
void Board::showArea(std::pair<size_t, size_t> target) const {
	char board_tmpl[13][27] = {
		{"\t    A B C D E F G H J K \n"},
		{"\t    _ _ _ _ _ _ _ _ _ _ \n"},
		{"\t 1 | | | | | | | | | | |\n"},
		{"\t 2 | | | | | | | | | | |\n"},
		{"\t 3 | | | | | | | | | | |\n"},
		{"\t 4 | | | | | | | | | | |\n"},
		{"\t 5 | | | | | | | | | | |\n"},
		{"\t 6 | | | | | | | | | | |\n"},
		{"\t 7 | | | | | | | | | | |\n"},
		{"\t 8 | | | | | | | | | | |\n"},
		{"\t 9 | | | | | | | | | | |\n"},
		{"\t10 | | | | | | | | | | |\n"},
		{"\t   `````````````````````\n"}
	};

	std::cout << '\n' << '\n' << '\n';
	std::cout << board_tmpl[0];
	std::cout << board_tmpl[1];

	for (int row = 0; row < size_; ++row) {
		for (int column = 0; column < size_; ++column) {
			if (area_[row][column] == true) {
				board_tmpl[ROW(row + 1)][COLUMN(column + 1)] = '~';
			}
		}
	}

	for (auto now : ships_) {
		int row = now.location_.second;
		int column = now.location_.first;
		for (int i = 0; i < now.type_; ++i) {
			if (area_[row - 1][column - 1] == true) {
				if (now.getStatus() == WATERED) {
					board_tmpl[ROW(row)][COLUMN(column)] = 'x';
				} else {
					board_tmpl[ROW(row)][COLUMN(column)] = '*';
				}
			}
			if (now.direction_ == HORIZONTAL) column++;
			else row++;
		}
	}

	if (target.first <= size_ && target.second <= size_ && target.first != 0 && target.second != 0) {
		board_tmpl[ROW(target.second)][COLUMN(target.first)] = '@';
	}

	for (int i = 2; i < 12; ++i) {
		std::cout << board_tmpl[i];
	}

	std::cout << board_tmpl[12];
}
bool Board::isLost() const {
	for (auto ship : ships_) {
		if (ship.getStatus() != WATERED) return false;
	}
	return true;
}
Ship Board::getShip(int index) const {
	return ships_[index];
}
