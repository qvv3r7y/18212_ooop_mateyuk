#include "Player.h"

#include <utility>

Player::Player(std::string name, StrategyType type, size_t size) {
	name_ = std::move(name);
	type_ = type;
	board_ = Board(size);
	switch (type) {
		case HUMAN:
			strategy_ = new HumanStrategy;
			break;
		case RANDOM:
			strategy_ = new RandomStrategy;
			break;
		case OPTIMAL:
			strategy_ = new OptimalStrategy;
			break;
	}

}
Player::~Player() {
	delete strategy_;
}
Board &Player::getBoard() {
	return board_;
}
void Player::prepareBoard() {

	strategy_->arrange(board_);

//	if (type_!= HUMAN) {
//		board_.showShips();
//		get_key();                          // TODO clean after debug
//		system("clear");
//	}

}
bool Player::move(Board &enemy_board) {
	bool hit = true;
	std::cout << "\t      " << name_ << "'s move.";
	while (hit) {
		hit = strategy_->shoot(enemy_board);
		if (enemy_board.isLost()) return true;
	}
	return false;
}
std::string Player::getName() const {
	return name_;
}
StrategyType Player::getType() {
	return type_;
}
