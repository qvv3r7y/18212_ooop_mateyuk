#include "Ship.h"

ShipStatus Ship::getStatus() const {
	return status_;
}
void Ship::setStatus(ShipStatus status) {
	status_ = status;
}
ShipType Ship::getType() const {
	return type_;
}
std::pair<size_t, size_t> Ship::getLocation() const {
	return location_;
}
size_t Ship::getDamages() const {
	return damages_;
}
ShipDirection Ship::getDirection() const {
	return direction_;
}
