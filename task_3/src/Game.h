#ifndef SEABATTLE_SRC_GAME_H_
#define SEABATTLE_SRC_GAME_H_

#include "Player.h"
#include "utility_foo.h"

class Game {
private:
public:
	Player *first_pl_;
	Player *second_pl_;
public:
	 explicit Game(size_t size = 10);
	~Game();
	static StrategyType option_menu();
	void start();
};

#endif //SEABATTLE_SRC_GAME_H_
