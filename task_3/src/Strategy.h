#ifndef SEABATTLE_SRC_STRATEGY_H_
#define SEABATTLE_SRC_STRATEGY_H_

#include <vector>
#include <iostream>

#include "utility_foo.h"
#include "Ship.h"
#include "Board.h"

#define SPEED_LEVEL "sleep 0.6"

#define QTY_SHIPS 10
#define QTY_MODES 3
#define SIZE 10

enum StrategyType {
	HUMAN = 0,
	RANDOM,
	OPTIMAL
};


struct finishing {
	std::pair<size_t, size_t> first_hit;
	std::pair<size_t, size_t> next_hit;
	std::pair<size_t, size_t> last_true_hit;
	MoveWay shoot_next;
	bool active = 0;
	bool last_shoot = true;
	int damages = 1;
};

class IStrategy {
public:
	virtual bool shoot(Board &board) = 0;
	virtual void arrange(Board &board) = 0;
	virtual ~IStrategy() = default;
};

class HumanStrategy : public IStrategy {
public:
	void arrange(Board &board) override;
	bool shoot(Board &board) override;
};

class RandomStrategy : public IStrategy {
private:
	std::vector<int> area;
public:
	RandomStrategy();
	void arrange(Board &board) override;
	bool shoot(Board &board) override;
	std::pair<size_t, size_t> make_rand_target();
};

class OptimalStrategy : public IStrategy {
private:
	std::vector<int> area;
	finishing finishing_;
public:
	OptimalStrategy();
	void arrange(Board &board) override;
	bool shoot(Board &board) override;
	bool findArea(std::pair<size_t,size_t> element, bool del = false);
	void setNextHit(MoveWay shoot_next = UP, bool set = false);
	std::pair<size_t, size_t> make_rand_target();
};

#endif //SEABATTLE_SRC_STRATEGY_H_
