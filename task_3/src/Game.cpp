#include "Game.h"

void Game::start() {
	if (first_pl_->getType() == HUMAN) {
		std::cout << "\t      " << first_pl_->getName() << " SET SHIPS";
	}
	first_pl_->prepareBoard();
	system("clear");
	if (second_pl_->getType() == HUMAN) {
		std::cout << "\t      " << second_pl_->getName() << " SET SHIPS";
	}
	second_pl_->prepareBoard();
	system("clear");

	while (true) {
		if (first_pl_->move(second_pl_->getBoard())) {
			first_pl_->getBoard().showShips();
			second_pl_->getBoard().showArea();
			std::cout << "\n\t\t" << first_pl_->getName() << " WIN!" << std::endl;
			break;
		}
		if (second_pl_->move(first_pl_->getBoard())) {
			second_pl_->getBoard().showShips();
			first_pl_->getBoard().showArea();
			std::cout << "\n\t\t" << second_pl_->getName() << " WIN!" << std::endl;
			break;
		}
	}

	std::cout << "\n\tPREES ANY KEY TO GAME OVER" << std::endl;
	get_key();
}

StrategyType Game::option_menu() {
	char menu[4][30] = {
		{"\t SELECT PLAYER\n"},
		{"> HUMAN\n"},
		{"  RANDOM AI   (easy)\n"},
		{"  OPTIMAL AI  (medium)\n"}
	};

	int mode = 1;
	int key = 0;

	for (auto &i : menu) {
		std::cout << i;
	}

	key = get_key();

	while (key != '\n') {
		menu[mode][0] = ' ';
		if (key == 'w' && mode != 1) {
			mode--;
		}
		if (key == 's' && mode != QTY_MODES) {
			mode++;
		}
		menu[mode][0] = '>';
		system("clear");
		for (auto &i : menu) {
			std::cout << i;
		}
		key = get_key();
	}
	system("clear");
	return static_cast<StrategyType>(mode - 1);
}
Game::Game(size_t size) {

	system("clear");
	StrategyType mode;
	mode = option_menu();
	if (mode == HUMAN) {
		std::string name;
		std::cout << "\nEnter name player_1: \n";
		std::cin >> name;
		getchar();
		first_pl_ = new Player(std::move(name), mode, size);
	} else if (mode == RANDOM) {
		first_pl_ = new Player("ROBO - RANDOM", mode, size);
	} else if (mode == OPTIMAL) {
		first_pl_ = new Player("ROBO - OPTIMAL", mode, size);
	}

	system("clear");
	mode = option_menu();
	if (mode == HUMAN) {
		std::string name;
		std::cout << "\nEnter name player_2: \n";
		std::cin >> name;
		getchar();
		second_pl_ = new Player(std::move(name), mode, size);
	} else if (mode == RANDOM) {
		second_pl_ = new Player("AI - RANDOM", mode, size);
	} else if (mode == OPTIMAL) {
		second_pl_ = new Player("AI - PRO", mode, size);
	}
	system("clear");
}
Game::~Game() {
	delete first_pl_;
	delete second_pl_;
}
