#include "Strategy.h"

void IStrategy::arrange(Board &board) {
	bool board_ship[SIZE + 2][SIZE + 2] = {{false}};
	bool success_move;
	size_t row = 0;
	size_t column = 0;
	ShipType type;
	ShipDirection direction;

	for (int i = 0; i < QTY_SHIPS; ++i) {
		do {
			success_move = true;
			type = board.getShip(i).getType();
			direction = static_cast<ShipDirection>(random(0, 1));
			if (direction == HORIZONTAL) {
				column = random(1, 11 - type);
				row = random(1, 10);
			} else {
				column = random(1, 10);
				row = random(1, 11 - type);
			}

			if (direction == HORIZONTAL) {
				for (int k = column; k < column + type; ++k) {
					if (board_ship[row][k] == true) {
						success_move = false;
						break;
					}
				}
			} else {
				for (int k = row; k < row + type; ++k) {
					if (board_ship[k][column] == true) {
						success_move = false;
						break;
					}
				}
			}

		} while (!success_move);

		board.moveShip(i, std::make_pair(column, row), direction);
		if (direction == HORIZONTAL) {
			row--;
			column--;
			for (int k = row; k < row + 3; ++k) {
				for (int j = column; j < column + type + 2; ++j) {
					board_ship[k][j] = true;
				}
			}
		} else {
			row--;
			column--;
			for (int k = row; k < row + type + 2; ++k) {
				for (int j = column; j < column + 3; ++j) {
					board_ship[k][j] = true;
				}
			}
		}
	}
}

void HumanStrategy::arrange(Board &board) {

	std::cout << "\n\tDo you want set randomly? PRESS Y/N";
	if (get_key() == 'y') {
		do {
			system("clear");
			IStrategy::arrange(board);
			board.showShips();
		} while (get_key() != '\n');
		return;
	}

	board.showShips();
	for (int i = 0; i < QTY_SHIPS; ++i) {
		int key = get_key();
		while (key != '\n') {
			switch (key) {
				case 'w':
					board.moveShip(i, UP, false);
					break;
				case 'a':
					board.moveShip(i, LEFT, false);
					break;
				case 's':
					board.moveShip(i, DOWN, false);
					break;
				case 'd':
					board.moveShip(i, RIGHT, false);
					break;
				case 'r':
					board.moveShip(i, UP, true);        /// crutch
					break;
				default:
					break;
			}
			system("clear");
			board.showShips();
			key = get_key();
		}
	}
}
bool HumanStrategy::shoot(Board &board) {
	std::pair<size_t, size_t> target = {1, 1};
	ShipStatus hit_status;

	board.showArea(target);
	int key = get_key();
	system("clear");

	while (true) {
		switch (key) {
			case 'w':
				if (target.second != 1) {
					target.second--;
				}
				break;
			case 'a':
				if (target.first != 1) {
					target.first--;
				}
				break;
			case 's':
				if (target.second != 10) {
					target.second++;
				}
				break;
			case 'd':
				if (target.first != 10) {
					target.first++;
				}
				break;
			case '\n':
				hit_status = board.setHit(std::move(target)).getStatus();
				board.showArea();
				if (hit_status == ALIVE) std::cout << "\n\t      Ooops... MISSED\n";
				if (hit_status == WATERED) std::cout << "\n\t   Yeah! SHIP WAS KILLED\n";
				if (hit_status == INJURED) std::cout << "\n\t    IT'S HIT! TRY AGAIN\n";
				get_key();
				system("clear");
				if (hit_status == ALIVE) return false;
				else return true;
			default:
				break;
		}
		board.showArea(target);
		key = get_key();
		system("clear");
	}
}

void RandomStrategy::arrange(Board &board) {
	IStrategy::arrange(board);
}

void OptimalStrategy::arrange(Board &board) {
	IStrategy::arrange(board);
}

bool RandomStrategy::shoot(Board &board) {
	ShipStatus hit_status;
	std::string speed_level = SPEED_LEVEL;

	std::pair<size_t, size_t> target = make_rand_target();

	board.showArea();
	system(speed_level.c_str());         /// CAN FASTER
	system(speed_level.c_str());         /// CAN FASTER
	system("clear");

	hit_status = board.setHit(std::move(target)).getStatus();
	board.showArea();
	if (hit_status == ALIVE) std::cout << "\n\t      Ooops... MISSED\n";
	if (hit_status == WATERED) std::cout << "\n\t   Yeah! SHIP WAS KILLED\n";
	if (hit_status == INJURED) std::cout << "\n\t    IT'S HIT! TRY AGAIN\n";
	system(speed_level.c_str());          /// CAN FASTER
	system("clear");
	if (hit_status == ALIVE) return false;
	else return true;
}


bool OptimalStrategy::shoot(Board &board) {
	std::pair<size_t, size_t> target; /// column, row
	ShipStatus hit_status;
	std::string speed_level = SPEED_LEVEL;

	if (finishing_.active == false) {
		target = make_rand_target();
	} else {
		findArea(finishing_.next_hit, true);
		target = finishing_.next_hit;
	}

	board.showArea();
	system(speed_level.c_str());         /// CAN FASTER
	system(speed_level.c_str());         /// CAN FASTER
	system("clear");

	Ship ship = board.setHit(target);
	hit_status = ship.getStatus();
	board.showArea();
	if (hit_status == ALIVE) std::cout << "\n\t      Ooops... MISSED\n";
	if (hit_status == WATERED) std::cout << "\n\t   Yeah! SHIP WAS KILLED\n";
	if (hit_status == INJURED) std::cout << "\n\t    IT'S HIT! TRY AGAIN\n";
	system(speed_level.c_str());          /// CAN FASTER
	system("clear");

	///

	if (hit_status == ALIVE) {
		if (finishing_.active == true) {
			finishing_.last_shoot = false;
			if (finishing_.damages == 1) {
				setNextHit();
			} else {
				switch (finishing_.shoot_next) {
					case UP:
						finishing_.shoot_next = DOWN;
						break;
					case DOWN:
						finishing_.shoot_next = UP;
						break;
					case RIGHT:
						finishing_.shoot_next = LEFT;
						break;
					case LEFT:
						finishing_.shoot_next = RIGHT;
						break;
				}
				setNextHit(finishing_.shoot_next, true);
			}
		}
		return false;
	}

	if (hit_status == INJURED) {
		finishing_.damages = ship.getDamages();
		finishing_.last_shoot = true;
		finishing_.last_true_hit = target;
		if (finishing_.damages == 1) {
			finishing_.active = true;
			finishing_.first_hit = target;
			setNextHit();
		} else {
			setNextHit(finishing_.shoot_next, true);
		}
		return true;
	}

	if (hit_status == WATERED) {
		finishing_.damages = 0;
		finishing_.active = false;
		finishing_.last_shoot = true;
		int row = ship.getLocation().second;
		int column = ship.getLocation().first;

		if (ship.getDirection() == HORIZONTAL) {
			row--;
			column--;
			for (int k = row; k < row + 3; ++k) {
				for (int j = column; j < column + ship.getType() + 2; ++j) {
					findArea(std::make_pair<size_t, size_t>(j, k), true);
				}
			}
		} else {
			row--;
			column--;
			for (int k = row; k < row + ship.getType() + 2; ++k) {
				for (int j = column; j < column + 3; ++j) {
					findArea(std::make_pair<size_t, size_t>(j, k), true);
				}
			}
		}
		return true;
	}

	return false;
}

void OptimalStrategy::setNextHit(MoveWay shoot_next, bool set) {
	while (true) {
		if (set == true) {
			finishing_.shoot_next = shoot_next;
		} else {
			finishing_.shoot_next = static_cast<MoveWay>(random(0, 3));
		}

		std::pair<size_t, size_t> new_pair = finishing_.last_true_hit;
		if (finishing_.last_shoot == false && finishing_.damages != 1) new_pair = finishing_.first_hit;
		switch (finishing_.shoot_next) {
			case UP:
				new_pair.second--;
				if (findArea(new_pair) == false) {
					if (set == true) {
						finishing_.last_true_hit = finishing_.first_hit;
						setNextHit(DOWN, true);
						return;
					}
					continue;
				}
				break;
			case DOWN:
				new_pair.second++;
				if (findArea(new_pair) == false) {
					if (set == true) {
						finishing_.last_true_hit = finishing_.first_hit;
						setNextHit(UP, true);
						return;
					}
					continue;
				}
				break;
			case RIGHT:
				new_pair.first++;
				if (findArea(new_pair) == false) {
					if (set == true) {
						finishing_.last_true_hit = finishing_.first_hit;
						setNextHit(LEFT, true);
						return;
					}
					continue;
				}
				break;
			case LEFT:
				new_pair.first--;
				if (findArea(new_pair) == false) {
					if (set == true) {
						finishing_.last_true_hit = finishing_.first_hit;
						setNextHit(RIGHT, true);
						return;
					}
					continue;
				}
				break;
		}
		finishing_.next_hit = new_pair;
		return;
	}
}

OptimalStrategy::OptimalStrategy() {
	area = std::vector<int>(SIZE * SIZE);
	for (int i = 0; i < SIZE * SIZE; ++i) {
		area[i] = i;
	}
}

RandomStrategy::RandomStrategy() {
	area = std::vector<int>(SIZE * SIZE);
	for (int i = 0; i < SIZE * SIZE; ++i) {
		area[i] = i;
	}
}
std::pair<size_t, size_t> RandomStrategy::make_rand_target() {
	std::pair<size_t, size_t> target;
	int hit = random(0, area.size() - 1);
	target.first = area[hit] / 10 + 1;
	target.second = area[hit] % 10 + 1;
	area.erase(area.begin() + hit);
	return target;
}

std::pair<size_t, size_t> OptimalStrategy::make_rand_target() {
	std::pair<size_t, size_t> target;
	int hit = random(0, area.size() - 1);
	target.first = area[hit] / 10 + 1;
	target.second = area[hit] % 10 + 1;
	area.erase(area.begin() + hit);
	return target;
}

bool OptimalStrategy::findArea(std::pair<size_t, size_t> element, bool del) {
	if (element.first == 0 || element.first > 10 || element.second == 0 || element.second > 10)
		return false;
	int find = (element.first - 1) * 10 + (element.second - 1);
	for (auto now = area.begin(); now != area.end(); now++) {
		if (*now == find) {
			if (del == true) area.erase(now);
			return true;
		}
	}
	return false;
}
