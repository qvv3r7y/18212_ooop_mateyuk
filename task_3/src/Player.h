#ifndef SEABATTLE_SRC_PLAYER_H_
#define SEABATTLE_SRC_PLAYER_H_

#include "Board.h"
#include "Strategy.h"

#include <string>
#include <utility>
#include <memory>

class Player {
private:
	std::string name_;
	Board board_;
	IStrategy *strategy_;
	StrategyType type_;
public:
	Player(std::string name, StrategyType type, size_t size = 10);
	~Player();
	void prepareBoard();
	bool move(Board &enemy_board);
	Board &getBoard();
	std::string getName() const;
	StrategyType getType();
};

#endif //SEABATTLE_SRC_PLAYER_H_
