#ifndef SEABATTLE_SRC_BOARD_H_
#define SEABATTLE_SRC_BOARD_H_

#include "Ship.h"
#include <vector>

#define ROW(i) ((i) + 1)
#define COLUMN(j) (2 * (j) + 3)

enum MoveWay {
	UP = 0,
	DOWN,
	RIGHT,
	LEFT
};

class Board {
private:
	bool area_[10][10] = {{0}};
	std::vector<Ship> ships_;
	size_t size_;
public:
	explicit Board(size_t size = 10);
	~Board();
	void moveShip(int index, MoveWay way, bool direction);
	void moveShip(int index, std::pair<size_t, size_t> place, ShipDirection direction);
	void showShips() const;
	Ship getShip(int index) const;
	void showArea(std::pair<size_t, size_t> target = {0, 0}) const;
	bool isLost() const;
	Ship setHit(std::pair<size_t, size_t> hit);
};


#endif //SEABATTLE_SRC_BOARD_H_
