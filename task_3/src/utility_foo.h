#ifndef SEABATTLE_SRC_UTILITY_FOO_H_
#define SEABATTLE_SRC_UTILITY_FOO_H_

#include <random>
#include <termios.h>
#include <unistd.h>
#include <iostream>

int get_key();
int random(int a, int b);

#endif //SEABATTLE_SRC_UTILITY_FOO_H_
